package Model;

import java.util.ArrayList;

    public class RssObject{
        public String status;
        public Feed feed;
        public ArrayList<Item> items;

        public RssObject(String status, Feed feed, ArrayList<Item> items) {
            this.status = status;
            this.feed = feed;
            this.items = items;
        }

        public String getStatus() {

            return status;
        }

        public void setStatus(String status) {

            this.status = status;
        }

        public Feed getFeed() {

            return feed;
        }

        public void setFeed(Feed feed) {

            this.feed = feed;
        }

        public ArrayList<Item> getItems() {

            return items;
        }

        public void setItems(ArrayList<Item> items) {

            this.items = items;
        }
    }

